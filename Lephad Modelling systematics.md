# ttbar, Z+HF and data-driven Fakes modelling uncertainties
## list for limit people
A list of shape uncertainties that need to be implemented in the limit.
Common convention:
Up: parametrization to the variation
Down: symmetrized parametrization
```
ttbar (SLT only):
ME:
SysTTBAR_ACC_GEN__1up
SysTTBAR_ACC_GEN__1down
PS:
SysTTBAR_ACC_PS__1up
SysTTBAR_ACC_PS__1down
```

```
Z+HF (SLT+LTT):
Scale:
SysZtautauClosurepTBB__1up
SysZtautauClosurepTBB__1down
```

```
singletop (SLT+LTT):
Interference
SysSingleTop_Wtchan_TopInterference__1up
SysSingleTop_Wtchan_TopInterference__1down
FSR:
SyssingletopMUR__1up
SyssingletopMUR__1down
```


## list of modelling systematics and the commands to run them
### ttbar modelling systematics:
FinalHHbbtautauLH   --fullyDataFakes 1 --splitTauFakes 1 --release 31  --pileupReweightingFromNtuple 1 -d root://eosatlas.cern.ch//eos/atlas/atlascerngroupdisk/phys-hdbs/diHiggs/bbtautau/lephad/Bulbasaur/SR/cluster_6/DATA1{6,7,8}/ -y {2016,2017,2018} -p 2 -q 1240 -s 999
For SLT. 
LTT channel is norm only, no shape dependence is assumed. 
### Z+HF modelling systematics
FinalHHbbtautauLH   --fullyDataFakes 1 --splitTauFakes 1 --release 31  --pileupReweightingFromNtuple 1 -d root://eosatlas.cern.ch//eos/atlas/atlascerngroupdisk/phys-hdbs/diHiggs/bbtautau/lephad/Bulbasaur/SR/cluster_6/DATA1{6,7,8}/ -y {2016,2017,2018} -p {21,22,23,24} -q 1243 -s 999
For SLT. 
To run for LTT channel simply add --ltt 1 at the end of the command.
### resonant signal systematics
FinalHHbbtautauLH   --fullyDataFakes 1  --release 31 --splitTauFakes 0 --pileupReweightingFromNtuple 1 -d root://eosatlas.cern.ch//eos/atlas/atlascerngroupdisk/phys-hdbs/diHiggs/bbtautau/lephad/Bulbasaur/SR/cluster_6/DATA1{6,7,8}/ -y {2016,2017,2018} -p 54 -q 1239 -s 999
For SLT. 
To run for LTT channel simply add --ltt 1 at the end of the command.
Notice we have --splitTauFakes 0 as we need both true/fake taus contribution.
### single top systematics:
FinalHHbbtautauLH   --fullyDataFakes 1  --release 31 --splitTauFakes 0 --pileupReweightingFromNtuple 1 -d root://eosatlas.cern.ch//eos/atlas/atlascerngroupdisk/phys-hdbs/diHiggs/bbtautau/lephad/Bulbasaur/SR/cluster_6/DATA1{6,7,8}/ -y {2016,2017,2018} -p 3 -q {1233,1235} -s 999
For SLT. 
To run for LTT channel simply add --ltt 1 at the end of the command.
Notice we have --splitTauFakes 0 as we need both true/fake taus contribution.

## Parametrization and closure

Description documented in:

[ttbar modelling systematics](https://gitlab.cern.ch/atlas-physics-office/HDBS/ANA-HDBS-2018-40/ANA-HDBS-2018-40-INT1/-/blob/master/sections/systs/modelling_ttbar.tex)

[Z+HF modelling systematics](https://gitlab.cern.ch/atlas-physics-office/HDBS/ANA-HDBS-2018-40/ANA-HDBS-2018-40-INT1/-/blob/master/sections/systs/modelling_ZHF.tex)

[data-driven Fakes systematics](https://gitlab.cern.ch/atlas-physics-office/HDBS/ANA-HDBS-2018-40/ANA-HDBS-2018-40-INT1/-/blob/master/sections/systs/datadriven_lephadfakes.tex)

[single top systematics](https://gitlab.cern.ch/atlas-physics-office/HDBS/ANA-HDBS-2018-40/ANA-HDBS-2018-40-INT1/-/blob/master/sections/systs/modelling_single_top.tex)
## Normalization variation table

[Full normalization variation table](https://docs.google.com/spreadsheets/d/1PUV90Eo12TvGPLCLwFgajJEyAMBeAJ1OC4puyROHI6Q/edit#gid=1526767178)

## Extrapolation Uncertainties

All extrapolation uncertainties are summarized in the
[spreadsheet](https://docs.google.com/spreadsheets/d/1yndLgXuvzlvjvUqhMwUaecDsAqu7HxrJ_1xBRSyKdOI/edit#gid=1891254703).

 

### ttbar
All ttbar systematics are shape only, normalization is droped in combined fit with lephad. 
#### Matrix Element

The variation is parametrized in pT of the 2 b-jets (at reco-level). 

```
SysTTbarClosureaMCpTBBMET__1up
SysTTbarClosureaMCpTBBMET__1down
```


The up variation is parametrizing the difference to `aMCAtNLO`. The down
variation is the symmetrized up variation.

Refer to documentation for plots and PNN score distribution of the parametrization.

#### Parton Shower

The variation is parametrized in leading b-jet pT (at
reco-level).

```
SysTTbarClosureHerwigpTB2mHH__1up
SysTTbarClosureHerwigpTB2mHH__1down
```



The up variation is parametrizing the difference to `Herwig7`. The down
variation is the symmetrized up variation.

Refer to documentation for plots and PNN score distribution of the parametrization.



#### ISR
The variation is norm only, refert to the extrapolation uncertainty table. 

#### FSR

The variation is using weights from the nominal sample and are therefore not
parametrized. 

`Notice the MUR__1up corresponds to the FSR down variation.`



#### PDF + `alpha_s`

The PDF + `alpha_s` variations have no impact on the shape of the MVA. Therefore
extrapolation uncertainties are sufficient.

Can be neglected in standalone fit. 


### Z+HF jets

Heavy Flavor components only: bb, bc, cc. All Z+HF systematics are shape only, normalization is droped in combined fit with lephad.


#### Scale variation

The envelop is parametrized in pT of the 2 b-jets (at reco-level).

```
SysZtautauClosurepTBB__1up
SysZtautauClosurepTBB__1down
```



Refer to documentation for plots and PNN score distribution of the parametrization.

#### intra-PDF and inter-PDF, alpha_s, ckkw & qsf uncertainties
There variations have no impact on the shape of the MVA. Therefore extrapolation uncertainties are sufficient.


### data-driven Fakes 
The data-driven fakes are estimated with the FF calculated with each ttbar variation and the data and other backround remaining unchanged. It's documented in the int-note for cross checks but not as input for the limit. No obvious shape dependence is observed in Fakes. However, unlike the ttbar or Z+HF uncertainties, the normalization of fakes is considered. Therefore please refer to [Full normalization variation table](https://docs.google.com/spreadsheets/d/1PUV90Eo12TvGPLCLwFgajJEyAMBeAJ1OC4puyROHI6Q/edit#gid=1526767178) for variation in normalization.
'Notice: The fakes modelling uncertainties need to be correlated to the ttbar modelling uncertainties!!'
i.e. aMC (matrix element) Fakes -> aMC ttbar, Herwig (parton shower) Fakes -> Herwig ttbar, etc.


#### Matrix Element
```
SLT: +13.84% LTT: +26.58%
```
#### Parton Shower
```
SLT: +14.00% LTT: +30.41%
```

#### ISR
##### ISR up
```
SLT: +31.12% LTT: +54.56%
```
##### ISR down
```
SLT: -29.65% LTT: -56.82%
```





